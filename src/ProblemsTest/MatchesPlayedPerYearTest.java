package ProblemsTest;

import Problems.MatchesPlayedPerYear;
import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class MatchesPlayedPerYearTest {

    @Test
    public void wrongPathException(){
        MatchesPlayedPerYear classOfMatchesPlayedPerYear = new MatchesPlayedPerYear();

        String actualResult = classOfMatchesPlayedPerYear.eachSeasonCount("src/MatchData/matchs.csv");

        String expectedResult = "File Not Found";

        assertEquals("Path File Not Found Assertion", expectedResult,actualResult);

    }

    @Test
    public void arrayIndexOutOfRangeException(){
        MatchesPlayedPerYear classOfMatchesPlayedPerYear = new MatchesPlayedPerYear();

        String actualResult = classOfMatchesPlayedPerYear.eachSeasonCount("src/MatchData/indexOutOfRange.txt");

        String expectedResult = "Index Out Of Range";

        assertTrue("Array Index Out Of Range", expectedResult == actualResult);

    }

    @Test
    public void assertFalseForException(){
        MatchesPlayedPerYear classOfMatchesPlayedPerYear = new MatchesPlayedPerYear();

        String actualResult = classOfMatchesPlayedPerYear.eachSeasonCount("src/MatchData/matches.csv");

        String expectedResult = "File Not Found";

        assertFalse("Assert False Exception For Correct Path", expectedResult == actualResult);

    }

    @Test
    public void nullException(){
        MatchesPlayedPerYear classOfMatchesPlayedPerYear = new MatchesPlayedPerYear();

        String actualResult = classOfMatchesPlayedPerYear.eachSeasonCount(null);

        assertNull("Passing Null",actualResult);
    }

    @Test
    public void notNullException(){
        MatchesPlayedPerYear classOfMatchesPlayedPerYear = new MatchesPlayedPerYear();

        String actualResult = classOfMatchesPlayedPerYear.eachSeasonCount("src/MatchData/matches.csv");

        assertNotNull("Checking Result Not Null", actualResult);
    }

    @Test
    public void returnType(){
        MatchesPlayedPerYear classOfMatchesPlayedPerYear = new MatchesPlayedPerYear();

        String actualResult = classOfMatchesPlayedPerYear.eachSeasonCount("src/MatchData/matches.csv");

        String expectedResult = "String";

        assertEquals("Checking Return Type", expectedResult,actualResult.getClass().getSimpleName());
    }

    @Test
    public void assertEqualsOfMatchesPlayedPerYear(){
        MatchesPlayedPerYear classOfMatchesPlayedPerYear = new MatchesPlayedPerYear();

        String actualResult = classOfMatchesPlayedPerYear.eachSeasonCount("src/MatchData/matches.csv");

        String expectedResult = "{2008=58, 2009=57, 2010=60, 2011=73, 2012=74, 2013=76, 2014=60, 2015=59, 2016=60, 2017=59}";

        assertEquals("Checking Final Result", expectedResult,actualResult);
    }

    @AfterClass
    public static void finalMessageOfTest(){
        System.out.println("Passed All TestCases in MatchesPlayedPerYear");
    }


}