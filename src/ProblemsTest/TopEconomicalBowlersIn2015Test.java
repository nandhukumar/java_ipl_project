package ProblemsTest;

import Problems.ExtraRunsConcededByTeamIn2016;
import Problems.TopEconomicalBowlersIn2015;
import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class TopEconomicalBowlersIn2015Test {

    String matchDataPath = "src/MatchData/matches.csv";
    String deliveryDataPath = "src/DeliveriesData/deliveries.csv";
    String year = "2015";

    @Test
    public void wrongPathException(){
        TopEconomicalBowlersIn2015 classOfTopEconomicalBowlersIn2015 = new TopEconomicalBowlersIn2015();

        String actualResult = classOfTopEconomicalBowlersIn2015.topEconomicalBowlersIn2015("src/MatchData/matchs.csv",deliveryDataPath,year);

        String expectedResult = "File Not Found";

        assertEquals("Path File Not Found Assertion", expectedResult,actualResult);

    }

    @Test
    public void arrayIndexOutOfRangeException(){
        TopEconomicalBowlersIn2015 classOfTopEconomicalBowlersIn2015 = new TopEconomicalBowlersIn2015();

        String actualResult = classOfTopEconomicalBowlersIn2015.topEconomicalBowlersIn2015("src/MatchData/indexOutOfRange.txt",deliveryDataPath,year);

        String expectedResult = "Index Out Of Range";

        assertTrue("Array Index Out Of Range", expectedResult == actualResult);

    }

    @Test
    public void assertFalseForException(){
        TopEconomicalBowlersIn2015 classOfTopEconomicalBowlersIn2015 = new TopEconomicalBowlersIn2015();

        String actualResult = classOfTopEconomicalBowlersIn2015.topEconomicalBowlersIn2015(matchDataPath,deliveryDataPath,year);

        String expectedResult = "File Not Found";

        assertFalse("Assert False Exception For Correct Path", expectedResult == actualResult);

    }

    @Test
    public void nullException(){
        TopEconomicalBowlersIn2015 classOfTopEconomicalBowlersIn2015 = new TopEconomicalBowlersIn2015();

        String actualResult = classOfTopEconomicalBowlersIn2015.topEconomicalBowlersIn2015(null,deliveryDataPath,year);

        assertNull("Passing Null",actualResult);
    }

    @Test
    public void notNullException(){
        TopEconomicalBowlersIn2015 classOfTopEconomicalBowlersIn2015 = new TopEconomicalBowlersIn2015();

        String actualResult = classOfTopEconomicalBowlersIn2015.topEconomicalBowlersIn2015(matchDataPath,deliveryDataPath,year);

        assertNotNull("Checking Result Not Null", actualResult);
    }

    @Test
    public void returnType(){
        TopEconomicalBowlersIn2015 classOfTopEconomicalBowlersIn2015 = new TopEconomicalBowlersIn2015();

        String actualResult = classOfTopEconomicalBowlersIn2015.topEconomicalBowlersIn2015(matchDataPath,deliveryDataPath,year);

        String expectedResult = "String";

        assertEquals("Checking Return Type", expectedResult,actualResult.getClass().getSimpleName());
    }

    @Test
    public void assertEqualsOfTopEconomicalBowlersIn2015(){
        TopEconomicalBowlersIn2015 classOfTopEconomicalBowlersIn2015 = new TopEconomicalBowlersIn2015();

        String actualResult = classOfTopEconomicalBowlersIn2015.topEconomicalBowlersIn2015(matchDataPath,deliveryDataPath,year);

        String expectedResult = "[RN ten Doeschate=4.0, J Yadav=4.14, R Ashwin=5.73, S Nadeem=6.14, Parvez Rasool=6.2, MC Henriques=6.31, Z Khan=6.36, MA Starc=6.8, M Vijay=7.0, Sandeep Sharma=7.04]";

        assertEquals("Checking Final Result", expectedResult,actualResult);
    }

    @AfterClass
    public static void finalMessageOfTest(){
        System.out.println("Passed All TestCases in Top10EconomicalBowlersIn2015");
    }

}