package ProblemsTest;

import Problems.ExtraRunsConcededByTeamIn2016;
import Problems.MatchesPlayedPerYear;
import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExtraRunsConcededByTeamIn2016Test {

    String matchDataPath = "src/MatchData/matches.csv";
    String deliveryDataPath = "src/DeliveriesData/deliveries.csv";
    String year = "2016";

    @Test
    public void wrongPathException(){
        ExtraRunsConcededByTeamIn2016 classOfExtraRunsConcededByTeamIn2016 = new ExtraRunsConcededByTeamIn2016();

        String actualResult = classOfExtraRunsConcededByTeamIn2016.extraRunsconcededByTeamInYear("src/MatchData/matchs.csv",deliveryDataPath,year);

        String expectedResult = "File Not Found";

        assertEquals("Path File Not Found Assertion", expectedResult,actualResult);

    }

    @Test
    public void arrayIndexOutOfRangeException(){
        ExtraRunsConcededByTeamIn2016 classOfExtraRunsConcededByTeamIn2016 = new ExtraRunsConcededByTeamIn2016();

        String actualResult = classOfExtraRunsConcededByTeamIn2016.extraRunsconcededByTeamInYear(matchDataPath,"src/MatchData/indexOutOfRange.txt",year);

        String expectedResult = "Index Out Of Range";

        assertTrue("Array Index Out Of Range", expectedResult == actualResult);

    }

    @Test
    public void assertFalseForException(){
        ExtraRunsConcededByTeamIn2016 classOfExtraRunsConcededByTeamIn2016 = new ExtraRunsConcededByTeamIn2016();

        String actualResult = classOfExtraRunsConcededByTeamIn2016.extraRunsconcededByTeamInYear(matchDataPath,deliveryDataPath,year);

        String expectedResult = "File Not Found";

        assertFalse("Assert False Exception For Correct Path", expectedResult == actualResult);

    }

    @Test
    public void nullException(){
        ExtraRunsConcededByTeamIn2016 classOfExtraRunsConcededByTeamIn2016 = new ExtraRunsConcededByTeamIn2016();

        String actualResult = classOfExtraRunsConcededByTeamIn2016.extraRunsconcededByTeamInYear(matchDataPath,null,null);

        assertNull("Passing Null",actualResult);
    }

    @Test
    public void notNullException(){
        ExtraRunsConcededByTeamIn2016 classOfExtraRunsConcededByTeamIn2016 = new ExtraRunsConcededByTeamIn2016();

        String actualResult = classOfExtraRunsConcededByTeamIn2016.extraRunsconcededByTeamInYear(matchDataPath,deliveryDataPath,year);

        assertNotNull("Checking Result Not Null", actualResult);
    }

    @Test
    public void returnType(){
        ExtraRunsConcededByTeamIn2016 classOfExtraRunsConcededByTeamIn2016 = new ExtraRunsConcededByTeamIn2016();

        String actualResult = classOfExtraRunsConcededByTeamIn2016.extraRunsconcededByTeamInYear(matchDataPath,deliveryDataPath,year);

        String expectedResult = "String";

        assertEquals("Checking Return Type", expectedResult,actualResult.getClass().getSimpleName());
    }

    @Test
    public void assertEqualsOfExtraRunsConcededByTeamIn2016(){
        ExtraRunsConcededByTeamIn2016 classOfExtraRunsConcededByTeamIn2016 = new ExtraRunsConcededByTeamIn2016();

        String actualResult = classOfExtraRunsConcededByTeamIn2016.extraRunsconcededByTeamInYear(matchDataPath,deliveryDataPath,year);

        String expectedResult = "{Delhi Daredevils=106, Gujarat Lions=98, Kings XI Punjab=100, Kolkata Knight Riders=122, Mumbai Indians=102, Rising Pune Supergiants=108, Royal Challengers Bangalore=156, Sunrisers Hyderabad=107}";

        assertEquals("Checking Final Result", expectedResult,actualResult);
    }

    @AfterClass
    public static void finalMessageOfTest(){
        System.out.println("Passed All TestCases in ExtraRunsConcededByTeamIn2016");
    }
}