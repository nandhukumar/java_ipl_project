package ProblemsTest;

import Problems.MatchesPlayedPerYear;
import Problems.NumberOfTimesTeamWonTheTossAndMatch;
import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class NumberOfTimesTeamWonTheTossAndMatchTest {

    @Test
    public void wrongPathException(){
        NumberOfTimesTeamWonTheTossAndMatch classOfNumberOfTimesTeamWonTheTossAndMatch = new NumberOfTimesTeamWonTheTossAndMatch();

        String actualResult = classOfNumberOfTimesTeamWonTheTossAndMatch.numberOfTimesTeamWonTheTossAndMatch("src/MatchData/matchs.csv");

        String expectedResult = "File Not Found";

        assertEquals("Path File Not Found Assertion", expectedResult,actualResult);

    }

    @Test
    public void arrayIndexOutOfRangeException(){
        NumberOfTimesTeamWonTheTossAndMatch classOfNumberOfTimesTeamWonTheTossAndMatch = new NumberOfTimesTeamWonTheTossAndMatch();

        String actualResult = classOfNumberOfTimesTeamWonTheTossAndMatch.numberOfTimesTeamWonTheTossAndMatch("src/MatchData/indexOutOfRange.txt");

        String expectedResult = "Index Out Of Range";

        assertTrue("Array Index Out Of Range", expectedResult == actualResult);

    }

    @Test
    public void assertFalseForException(){
        NumberOfTimesTeamWonTheTossAndMatch classOfNumberOfTimesTeamWonTheTossAndMatch = new NumberOfTimesTeamWonTheTossAndMatch();

        String actualResult = classOfNumberOfTimesTeamWonTheTossAndMatch.numberOfTimesTeamWonTheTossAndMatch("src/MatchData/matches.csv");

        String expectedResult = "File Not Found";

        assertFalse("Assert False Exception For Correct Path", expectedResult == actualResult);

    }

    @Test
    public void nullException(){
        NumberOfTimesTeamWonTheTossAndMatch classOfNumberOfTimesTeamWonTheTossAndMatch = new NumberOfTimesTeamWonTheTossAndMatch();

        String actualResult = classOfNumberOfTimesTeamWonTheTossAndMatch.numberOfTimesTeamWonTheTossAndMatch(null);

        assertNull("Passing Null",actualResult);
    }

    @Test
    public void notNullException(){
        NumberOfTimesTeamWonTheTossAndMatch classOfNumberOfTimesTeamWonTheTossAndMatch = new NumberOfTimesTeamWonTheTossAndMatch();

        String actualResult = classOfNumberOfTimesTeamWonTheTossAndMatch.numberOfTimesTeamWonTheTossAndMatch("src/MatchData/matches.csv");

        assertNotNull("Checking Result Not Null", actualResult);
    }

    @Test
    public void returnType(){
        NumberOfTimesTeamWonTheTossAndMatch classOfNumberOfTimesTeamWonTheTossAndMatch = new NumberOfTimesTeamWonTheTossAndMatch();

        String actualResult = classOfNumberOfTimesTeamWonTheTossAndMatch.numberOfTimesTeamWonTheTossAndMatch("src/MatchData/matches.csv");

        String expectedResult = "String";

        assertEquals("Checking Return Type", expectedResult,actualResult.getClass().getSimpleName());
    }

    @Test
    public void assertEqualsOfNumberOfTimesTeamWonTheTossAndMatch(){
        NumberOfTimesTeamWonTheTossAndMatch classOfNumberOfTimesTeamWonTheTossAndMatch = new NumberOfTimesTeamWonTheTossAndMatch();

        String actualResult = classOfNumberOfTimesTeamWonTheTossAndMatch.numberOfTimesTeamWonTheTossAndMatch("src/MatchData/matches.csv");

        String expectedResult = "{Chennai Super Kings=42, Deccan Chargers=19, Delhi Daredevils=33, Gujarat Lions=10, Kings XI Punjab=28, Kochi Tuskers Kerala=4, Kolkata Knight Riders=44, Mumbai Indians=48, Pune Warriors=3, Rajasthan Royals=34, Rising Pune Supergiant=5, Rising Pune Supergiants=3, Royal Challengers Bangalore=35, Sunrisers Hyderabad=17}";

        assertEquals("Checking Final Result", expectedResult,actualResult);
    }

    @AfterClass
    public static void finalMessageOfTest(){
        System.out.println("Passed All TestCases in NumberOfTimesTeamWonTheTossAndMatch");
    }


}