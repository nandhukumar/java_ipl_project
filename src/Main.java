

import ProblemsTest.JUnitTestSuite;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

// Test runner is used for executing the test cases.

public class Main {
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(JUnitTestSuite.class);

        for(Failure failure : result.getFailures()){
            System.out.println("Failed");
            System.out.println(failure);
        }

        System.out.println("Result = " + result.wasSuccessful());
    }
}
